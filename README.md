# openresty-links



## Getting started

```
podman build -f openresty-links/Dockerfile -t openresty-links openresty-links
```

```
podman run -p 8080:80 -e GITLAB_USERNAME=some-user-name -e EMAIL_ADDRESS=contact@example.fr -e LINKEDIN_USERNAME=other-user-name openresty-links
```

## examples
```
curl http://localhost:8080/gitlab -v
* Host localhost:8080 was resolved.
* IPv6: ::1
* IPv4: 127.0.0.1
*   Trying [::1]:8080...
* Connected to localhost (::1) port 8080
> GET /gitlab HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.6.0
> Accept: */*
>
< HTTP/1.1 302 Moved Temporarily
< Server: openresty/1.25.3.1
< Date: Mon, 17 Jun 2024 13:06:44 GMT
< Content-Type: text/html
< Content-Length: 151
< Connection: keep-alive
< Location: https://gitlab.com/some-user-name
<
<html>
<head><title>302 Found</title></head>
<body>
<center><h1>302 Found</h1></center>
<hr><center>openresty/1.25.3.1</center>
</body>
</html>
* Connection #0 to host localhost left intact
```


```
curl http://localhost:8080/email -v
* Host localhost:8080 was resolved.
* IPv6: ::1
* IPv4: 127.0.0.1
*   Trying [::1]:8080...
* Connected to localhost (::1) port 8080
> GET /email HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.6.0
> Accept: */*
>
< HTTP/1.1 302 Moved Temporarily
< Server: openresty/1.25.3.1
< Date: Mon, 17 Jun 2024 13:10:25 GMT
< Content-Type: text/html
< Content-Length: 151
< Connection: keep-alive
< Location: mailto:contact@contact@contact@example.fr?subject=Hello
<
<html>
<head><title>302 Found</title></head>
<body>
<center><h1>302 Found</h1></center>
<hr><center>openresty/1.25.3.1</center>
</body>
</html>
* Connection #0 to host localhost left intact
```