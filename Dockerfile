FROM openresty/openresty:alpine

COPY conf.d/nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf
